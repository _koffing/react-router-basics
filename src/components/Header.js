import React from 'react';
import { NavLink } from 'react-router-dom';

const Header = () => (
  <header>
    <span className="icn-logo"><i className="material-icons">code</i></span>
    <ul className="main-nav">
      <li><NavLink exact activeClassName="active" activeStyle={{background: 'pink'}} to="/">Home</NavLink></li>
      <li><NavLink exact activeClassName="active" to="/about">About</NavLink></li>
      <li><NavLink exact activeClassName="active" to="/teachers">Teachers</NavLink></li>
      <li><NavLink exact activeClassName="active" to="/courses">Courses</NavLink></li>
    </ul>    
  </header>
);

export default Header;